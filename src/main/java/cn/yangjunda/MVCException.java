package cn.yangjunda;

/**
 * @author Juanda
 * @Description
 * @date 2017/12/19 13:12
 */
public class MVCException extends RuntimeException {

    public MVCException(String msg) {
        super(msg);
    }

    public MVCException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
