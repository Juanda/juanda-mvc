package cn.yangjunda.util;



import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;


/**
 * Created by Juanda on 2017/5/3.
 */
public class PropertiesUtil {



    private static Properties props;

    static {
        String fileName = "juanda-mvc.properties";
        props = new Properties();
        try {
            props.load(new InputStreamReader(PropertiesUtil.class.getClassLoader().getResourceAsStream(fileName),"UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException("配置文件读取异常");
        }
    }


    public static String getProperty(String key){
        String value = props.getProperty(key.trim());
        if(StringUtils.isBlank(value)){
            return null;
        }
        return value.trim();
    }

    public static String getProperty(String key,String defaultValues){

        String value = props.getProperty(key.trim());
        if(StringUtils.isBlank(value)){
            value = defaultValues;
        }
        return value.trim();
    }

}
