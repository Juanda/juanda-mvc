package cn.yangjunda.util;

import java.math.BigDecimal;

/**
 * @author Juanda
 * @Description
 * @date 2018/7/22 13:43
 */
public class ParseObjectUtil {

    public static Object stringToObject(Class<?> paramType, String value) {
        Object object = null;
        if (paramType == String.class) {
            object = value;
        } else if (paramType == Integer.class) {
            if(StringUtils.isBlank(value)){
                object=null;
            }else {
                object = Integer.valueOf(value);
            }
        } else if(paramType == int.class){
            object = Integer.parseInt(value);
        } else if (paramType == Long.class || paramType == long.class) {
            object = Long.parseLong(value);
        } else if (paramType == Boolean.class || paramType == boolean.class) {
            object = Boolean.parseBoolean(value);
        } else if (paramType == Short.class || paramType == short.class) {
            object = Short.parseShort(value);
        } else if (paramType == Float.class || paramType == float.class) {
            object = Float.parseFloat(value);
        } else if (paramType == Double.class || paramType == double.class) {
            object = Double.parseDouble(value);
        } else if (paramType == BigDecimal.class) {
            object = new BigDecimal(value);
        } else if (paramType == Character.class || paramType == char.class) {
            char[] cs = value.toCharArray();
            if (cs.length > 1) {
                throw new IllegalArgumentException("参数长度太大");
            }
            object = value.toCharArray()[0];
        }
        return object;
    }
}
