package cn.yangjunda.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by juanda on 12/9/17.
 */
public class Configuration {

    private String packagePath;

    private Boolean autowire = true;

    private Map<String,Object> beans = new HashMap<>();

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public Boolean getAutowire() {
        return autowire;
    }

    public void setAutowire(Boolean autowire) {
        this.autowire = autowire;
    }

    public Map<String, Object> getBeans() {
        return beans;
    }

    public void setBeans(Map<String, Object> beans) {
        this.beans = beans;
    }

    public void setBean(String id, Object object) {
        beans.put(id, object);
    }

    public Object getBean(String id) {
        return this.beans.get(id);
    }
}
