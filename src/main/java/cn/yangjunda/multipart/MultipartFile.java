package cn.yangjunda.multipart;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by juanda on 12/15/17.
 */
public interface MultipartFile {

    String getOriginalFilename();

    String getContentType();

    boolean isEmpty();

    long getSize();

    byte[] getBytes() throws IOException;

    InputStream getInputStream() throws IOException;

    void transferTo(File file) throws IOException, IllegalStateException;
}
