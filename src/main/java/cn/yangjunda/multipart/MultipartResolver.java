package cn.yangjunda.multipart;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Juanda
 * @Description
 * @date 2017/12/19 9:24
 */
public interface MultipartResolver {

    int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
    int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB

    MultipartFile resolveMultipart(HttpServletRequest request);

}
