package cn.yangjunda.multipart.exception;


/**
 * @author Juanda
 * @Description
 * @date 2017/12/19 9:56
 */
public class MultipartException  extends RuntimeException {


    public MultipartException(String msg) {
        super(msg);
    }

    public MultipartException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
