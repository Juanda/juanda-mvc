package cn.yangjunda.multipart.exception;


/**
 * @author Juanda
 * @Description
 * @date 2017/12/19 9:57
 */
@SuppressWarnings("serial")
public class MaxUploadSizeExceededException extends MultipartException {

    private final long maxUploadSize;

    public MaxUploadSizeExceededException(long maxUploadSize) {
        this(maxUploadSize, null);
    }


    public MaxUploadSizeExceededException(long maxUploadSize, Throwable ex) {
        super("Maximum upload size " + (maxUploadSize >= 0 ? "of " + maxUploadSize + " bytes " : "") + "exceeded", ex);
        this.maxUploadSize = maxUploadSize;
    }


    public long getMaxUploadSize() {
        return this.maxUploadSize;
    }

}
