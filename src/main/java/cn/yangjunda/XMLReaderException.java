package cn.yangjunda;

/**
 * @author Juanda
 * @Description
 * @date 2017/12/22 13:32
 */
public class XMLReaderException extends RuntimeException {

    public XMLReaderException(String msg) {
        super(msg);
    }

    public XMLReaderException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
