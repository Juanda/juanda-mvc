package cn.yangjunda.annotation;

import java.lang.annotation.*;

/**
 * Created by juanda on 12/7/17.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JuandaResponseBody {
}
