package cn.yangjunda.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * @Description 用作请求传参数的别名
 * @author juanda
 * @date 2017年11月30日下午13:12:36
 *
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestObject {

	/**
	 * 表示参数的别名，必填
	 * @return
	 */
	String value();
}
